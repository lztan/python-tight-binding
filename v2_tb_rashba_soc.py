#!/usr/bin/env  python2


import sys
import numpy as np


class tb_soc:

    def __init__( self, norb, lattice, orbitals, kpt, onsite_energy, hoppings, lorb=None, z_hop=None, soc=True, eigenvector=True, auto_orb=False ):

        self._norb   = norb
        self._auto   = auto_orb
        self._lat    = lattice
        self._orb    = orbitals
        self._e_orb  = onsite_energy
        self._kpt    = kpt
        self._nkpt   = np.shape(self._kpt)[0]
        self._hop    = hoppings
        self._soc    = soc
        if self._soc == False and self._auto == True:
            raise Exception ('auto_orb should be set to false')
        if self._norb != np.shape( self._orb )[0]: 
            raise Exception ('norb or orb[] set to wrong')
        if self._soc == True and self._auto == True:
            self._norb = self._norb * 2
            self._orb = np.vstack( (self._orb, self._orb) )
            self._e_orb = np.concatenate( (self._e_orb, self._e_orb) )
        if self._soc == True:
            if lorb  != None: 
                self._lorb  = lorb
            else:
                self._lorb  = None
            if z_hop != None: 
                self._z_hop = z_hop
            else:
                self._z_hop = None
        self._eval  =  np.zeros((self._norb,self._nkpt)).astype(float)
        self._evc   =  np.zeros((self._norb,self._nkpt,self._norb)).astype(complex)
        self._eigenvectors = eigenvector
           



    def gen_ham_onsite( self, kpt_coord ):
        ham_tmp = np.zeros((  self._norb,  self._norb)).astype(complex)
        if self._soc == False or (self._soc == True and self._auto == False):
            for iorb in range(0,self._norb):
                ham_tmp[iorb,iorb] = self._e_orb[iorb]
        elif self._soc == True and self._auto == True:
            for iorb in range(0,self._norb/2):
                ham_tmp[iorb,iorb] = self._e_orb[iorb]
                ham_tmp[iorb+self._norb/2,iorb+self._norb/2] = self._e_orb[iorb]
        return  ham_tmp




    def gen_ham_hopping( self, kpt_coord ):
        ham_tmp = np.zeros((self._norb,self._norb)).astype(complex)
        if self._soc == False or (self._soc == True and self._auto == False):
            for hopping in self._hop:
                amp = complex(hopping[0])
                i   = int(hopping[1])
                j   = int(hopping[2])
                ind_R = np.array(hopping[3],dtype=float)
                rijR = -self._orb[i,:]+self._orb[j,:]+ind_R
                phase = np.exp((2.0j)*np.pi*np.dot(kpt_coord,rijR))
                amp = amp*phase
                ham_tmp[i,j] += amp
                ham_tmp[j,i] += amp.conjugate()
        elif self._soc == True and self._auto == True:
            for hopping in self._hop:
                amp = complex(hopping[0])
                i   = int(hopping[1])
                j   = int(hopping[2])
                ind_R = np.array(hopping[3],dtype=float)
                rijR = -self._orb[i,:]+self._orb[j,:]+ind_R
                phase = np.exp((2.0j)*np.pi*np.dot(kpt_coord,rijR))
                amp = amp*phase
                ham_tmp[i,j] += amp
                ham_tmp[j,i] += amp.conjugate()
                ham_tmp[i+self._norb/2,j+self._norb/2] += amp
                ham_tmp[j+self._norb/2,i+self._norb/2] += amp.conjugate()
        return  ham_tmp



    def gen_ham_ls( self, kpt_coord ):
        # only couples p-p orbitals
        if self._soc == False or self._lorb is None:
            return
        else:
            ham_tmp = np.zeros((self._norb,self._norb)).astype(complex)
            for ilorb in self._lorb:
                amp = complex(ilorb[0])
                i   = int(ilorb[1])
                j   = int(ilorb[2])
                ind_R = np.array(ilorb[3],dtype=float)
                rijR = -self._orb[i,:]+self._orb[j,:]+ind_R
                phase = np.exp((2.0j)*np.pi*np.dot(kpt_coord,rijR))
                amp = amp*phase
                ham_tmp[i,j] += amp
                ham_tmp[j,i] += amp.conjugate()
        return  ham_tmp



    def gen_ham_z_hop( self, kpt_coord ):
        # with auto z_hop 
        if self._z_hop is None:
            return
        else:
            ham_tmp = np.zeros((self._norb,self._norb)).astype(complex)
            if self._soc == False or (self._soc == True and self._auto == False):
                if len(self._z_hop) == 0: return ham_tmp
                for hopping in self._z_hop:
                    amp = complex(hopping[0])
                    i   = int(hopping[1])
                    j   = int(hopping[2])
                    ind_R = np.array(hopping[3],dtype=float)
                    rijR = -self._orb[i,:]+self._orb[j,:]+ind_R
                    phase = np.exp((2.0j)*np.pi*np.dot(kpt_coord,rijR))
                    amp = amp*phase
                    ham_tmp[i,j] += amp
                    ham_tmp[j,i] += amp.conjugate()
            elif self._soc == True and self._auto == True:
                if len(self._z_hop) == 0: return ham_tmp
                for hopping in self._z_hop:
                    amp = complex(hopping[0])
                    i   = int(hopping[1])
                    j   = int(hopping[2])
                    ind_R = np.array(hopping[3],dtype=float)
                    rijR = -self._orb[i,:]+self._orb[j,:]+ind_R
                    phase = np.exp((2.0j)*np.pi*np.dot(kpt_coord,rijR))
                    amp = amp*phase
                    ham_tmp[i,j] += amp
                    ham_tmp[j,i] += amp.conjugate()
                    ham_tmp[i+self._norb/2,j+self._norb/2] += amp
                    ham_tmp[j+self._norb/2,i+self._norb/2] += amp.conjugate()
        return  ham_tmp




    def gen_ham( self, k ):
        if self._soc == True:
            if self._z_hop is None:
                ham  =  self.gen_ham_onsite(k)  +  self.gen_ham_hopping(k)  +  self.gen_ham_ls(k)
            else:
                ham  =  self.gen_ham_onsite(k)  +  self.gen_ham_hopping(k)  +  self.gen_ham_ls(k)  +  self.gen_ham_z_hop(k)
        else:
            ham  =  self.gen_ham_onsite(k)  +  self.gen_ham_hopping(k)  
        return ham




    def display_ham( self, k ):
        if self._soc == True:
            ham  =  self.gen_ham_onsite(k)  +  self.gen_ham_hopping(k)  +  self.gen_ham_ls(k)  +  self.gen_ham_z_hop(k)
        else:
            ham  =  self.gen_ham_onsite(k)  +  self.gen_ham_hopping(k)  
#       for i in range(0,self._norb):
#           print ham[i,:]
#       np.savetxt('ham',ham[:5,:5],fmt='%10.6f')




    def solve_all(self,eig_vectors=False):
        for i in range(0,self._nkpt):
            k = self._kpt[i,:]
            ham=self.gen_ham(k)
            if eig_vectors==False:
                eval = _sol_ham(ham,eig_vectors=eig_vectors)
                self._eval[:,i]=eval[:]
            else:
                (eval,evec) = _sol_ham(ham,eig_vectors=eig_vectors)
                self._eval[:,i]=eval[:]
                self._evc [:,i,:]=evec[:,:]
        return 






def _sol_ham(ham,eig_vectors=False):
    if eig_vectors==False: 
        eval=np.linalg.eigvals(ham)
        # sort evectors, eigenvalues and convert to real numbers
        eval=_nicefy_eig(eval)
        return np.array(eval,dtype=float)
    else: 
        (eval,eig)=np.linalg.eigh(ham)
        # transpose matrix eig since otherwise it is confusing
        # now eig[i,:] is eigenvector for eval[i]-th eigenvalue
        eig=eig.T
        # sort evectors, eigenvalues and convert to real numbers
        (eval,eig)=_nicefy_eig(eval,eig)
        return (eval,eig)

def _nicefy_eig(eval,eig=None):
    "Sort eigenvaules and eigenvectors, if given, and convert to real numbers"
    # first take only real parts of the eigenvalues
    np.set_printoptions(precision=3)
#   print eval
    eval=np.array(eval.real,dtype=float)
    # sort energies
    args=eval.argsort()
    eval=eval[args]
    if eig is not None:
        eig=eig[args]
        return (eval,eig)
    return eval

def _nice_float(x,just,rnd):
    return str(round(x,rnd)).rjust(just)

def _nice_int(x,just):
    return str(x).rjust(just)

def k_path(kpts,nk,endpoint=True):
    """
        path = [[0.0, 0.0], [0.0, 0.5], [0.5, 0.5], [0.0, 0.0]]
        kpts = k_path(path, 100)

    """
    kint=[]
    k_list=np.array(kpts)
    for i in range(len(k_list)-1):
        for j in range(nk):
            cur=k_list[i]+(k_list[i+1]-k_list[i])*float(j)/float(nk)
            kint.append(cur)
    if endpoint==True:
        kint.append(k_list[-1])
    kint=np.array(kint)
    return kint






