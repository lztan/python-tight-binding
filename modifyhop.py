
import numpy as np

def superlattice(hop0,n1,n2,n3):
  """ creates hoppings for a n1 x n2 x n3 supercell."""
 
  #count the number of orbitals
  orbs = dict([])
  for h in hop0:
    orbs[h[1]] = 0
    orbs[h[2]] = 0
  norb = len(orbs)

  #create new hoppings
  hop1 = []
  for h in hop0:
    oldfrom = h[1]
    oldto = h[2]
    d = h[3]
    for i1 in range(n1):
      for i2 in range(n2):
        for i3 in range(n3):

          j1 = (i1+d[0])%n1
          j2 = (i2+d[1])%n2
          j3 = (i3+d[2])%n3

          newfrom = ( i1*n2*n3 + i2*n3 + i3 )*norb + oldfrom
          newto = ( j1*n2*n3 + j2*n3 + j3 )*norb + oldto

          newd1 = (i1+d[0])/n1
          newd2 = (i2+d[1])/n2
          newd3 = (i3+d[2])/n3
          newd = [newd1,newd2,newd3]

          hop1.append([h[0], newfrom, newto, newd])

  return hop1


def finitesys(hop0):
  """ removes periodic boundary conditions. """

  hop1 = []
  for h in hop0:
    d1 = h[2][0]
    d2 = h[2][1]
    d3 = h[2][2]
    if d1==0 and d2==0 and d3==0:
      hop1.append(h)

  return hop1

def combinehop(hop0,hop1,hop01):
  """ combines two systems. """

  #count the number of orbitals
  orbs0 = dict([])
  for h in hop0:
    orbs0[h[1]] = 0
    orbs0[h[2]] = 0
  norb0 = len(orbs0)

  orbs1 = dict([])
  for h in hop1:
    orbs1[h[1]] = 0
    orbs1[h[2]] = 0
  norb1 = len(orbs1)

  #new hoppings
  hop2 = []
  for h in hop0:
    hop2.append(h)

  for h in hop1:
    newh = [h[0], h[1]+norb1, h[2]+norb1, h[3]]
    hop2.append(newh)

  for h in hop01:
    newh = [h[0], h[1], h[2]+norb1, h[3]]
    hop2.append(newh)

  return hop2






