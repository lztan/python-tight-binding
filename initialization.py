#!/usr/bin/env  python2

import numpy as np
from math import sqrt, exp


class parasinitial:


    def __init__( self,hl ,lattice=None,orbitals=None,onsite=None ):
# lattice  Angstrom
        if lattice is None:
            raise Exception('no lattice input')
        else:
            self.lattice         = lattice

# orbitals
        self.orb_list_dic    =  { 0:'Pb_s_up', 1:'Pb_px_up', 2:'Pb_py_up', 3:'Pb_pz_up', 4:'I1_px_up', 5:'I1_py_up', 6:'I1_pz_up', 7:'I2_px_up', 8:'I2_py_up', 9:'I2_pz_up',10:'I3_px_up',11:'I3_py_up',12:'I3_pz_up',
                     13:'Pb_s_dw',14:'Pb_px_dw',15:'Pb_py_dw',16:'Pb_pz_dw',17:'I1_px_dw',18:'I1_py_dw',19:'I1_pz_dw',20:'I2_px_dw',21:'I2_py_dw',22:'I2_pz_dw',23:'I3_px_dw',24:'I3_py_dw',25:'I3_pz_dw'}
        self.orb_type_dic    =  { 0:'s', 1:'px', 2:'py', 3:'pz', 4:'px', 5:'py', 6:'pz', 7:'px', 8:'py', 9:'pz',10:'px',11:'py',12:'pz',13:'s',14:'px',15:'py',16:'pz',17:'px',18:'py',19:'pz',20:'px',21:'py',22:'pz',23:'px',24:'py',25:'pz'}
        if orbitals is None:
            raise Exception('no orbital coord input')
        else:
            self.orbitals        = orbitals
        self.norb            = len(self.orbitals)

# onsite energy
        if onsite is None:
            raise Exception('no onsite energy input')
        else:
            self.onsite          = onsite

# orbitals coordinates in cartesian
        self.orb_coords = np.dot( self.orbitals, self.lattice )

# read the high symmetry structure
        self.high_lattice = np.loadtxt('high_lattice',dtype=float)

# read the high symmetry structure
        self.high_orbitals = np.loadtxt('high_orbitals',dtype=float)

# high symmetry structure orbital coordinates in cartesian
        self.high_orb_coords = np.dot( self.high_orbitals, self.high_lattice )

# hopping decay factor
        self.alpha = {'spsi':0.8000, 'ppsi':1.26793, 'pppi':0.50000}


# hoppings
        self.hl       = hl

        tspsi=-1.014
        tpppi=-0.526
        tppsi= 1.826
        sBp=   0.538
        sAp=  -0.338

        d  = {0:[0,0,0],1:[1,0,0],2:[0,1,0],3:[0,0,1]}



        self.t_hoppings      = [ [tspsi* self.b( 0, 4,d[0], 0, 1,'spsi'), 0, 4, d[0]],
                                 [tspsi* self.b( 0, 8,d[0], 0, 2,'spsi'), 0, 8, d[0]],
                                 [tspsi* self.b( 0,12,d[0], 0, 3,'spsi'), 0,12, d[0]],
                                 [tppsi* self.b( 1, 4,d[0], 1, 1,'ppsi'), 1, 4, d[0]],
                                 [tpppi* self.b( 1, 4,d[0], 1, 1,'pppi'), 1, 4, d[0]],
                                 [tppsi* self.b( 2, 8,d[0], 2, 2,'ppsi'), 2, 8, d[0]],
                                 [tpppi* self.b( 2, 8,d[0], 2, 2,'pppi'), 2, 8, d[0]],
                                 [tppsi* self.b( 3,12,d[0], 3, 3,'ppsi'), 3,12, d[0]],
                                 [tpppi* self.b( 3,12,d[0], 3, 3,'pppi'), 3,12, d[0]],
                                 [tpppi* self.b( 2, 5,d[0], 2, 2,'pppi'), 2, 5, d[0]],
                                 [tppsi* self.b( 2, 5,d[0], 2, 2,'ppsi'), 2, 5, d[0]],
                                 [tpppi* self.b( 2,11,d[0], 2, 2,'pppi'), 2,11, d[0]],
                                 [tppsi* self.b( 2,11,d[0], 2, 2,'ppsi'), 2,11, d[0]],
                                 [tpppi* self.b( 1, 7,d[0], 1, 1,'pppi'), 1, 7, d[0]],
                                 [tppsi* self.b( 1, 7,d[0], 1, 1,'ppsi'), 1, 7, d[0]],
                                 [tpppi* self.b( 1,10,d[0], 1, 1,'pppi'), 1,10, d[0]],
                                 [tppsi* self.b( 1,10,d[0], 1, 1,'ppsi'), 1,10, d[0]],
                                 [tpppi* self.b( 3, 6,d[0], 3, 3,'pppi'), 3, 6, d[0]],
                                 [tppsi* self.b( 3, 6,d[0], 3, 3,'ppsi'), 3, 6, d[0]],
                                 [tpppi* self.b( 3, 9,d[0], 3, 3,'pppi'), 3, 9, d[0]],
                                 [tppsi* self.b( 3, 9,d[0], 3, 3,'ppsi'), 3, 9, d[0]],
                                 [tspsi* self.b( 4, 0,d[1], 1, 0,'spsi'), 4, 0, d[1]],
                                 [tspsi* self.b( 8, 0,d[2], 2, 0,'spsi'), 8, 0, d[2]],
                                 [tspsi* self.b(12, 0,d[3], 3, 0,'spsi'),12, 0, d[3]],
                                 [tppsi* self.b( 4, 1,d[1], 1, 1,'ppsi'), 4, 1, d[1]],
                                 [tpppi* self.b( 4, 1,d[1], 1, 1,'pppi'), 4, 1, d[1]],
                                 [tppsi* self.b( 8, 2,d[2], 2, 2,'ppsi'), 8, 2, d[2]],
                                 [tpppi* self.b( 8, 2,d[2], 2, 2,'pppi'), 8, 2, d[2]],
                                 [tppsi* self.b(12, 3,d[3], 3, 3,'ppsi'),12, 3, d[3]],
                                 [tpppi* self.b(12, 3,d[3], 3, 3,'pppi'),12, 3, d[3]],
                                 [tpppi* self.b( 5, 2,d[1], 2, 2,'pppi'), 5, 2, d[1]],
                                 [tppsi* self.b( 5, 2,d[1], 2, 2,'ppsi'), 5, 2, d[1]],
                                 [tpppi* self.b(11, 2,d[3], 2, 2,'pppi'),11, 2, d[3]],
                                 [tppsi* self.b(11, 2,d[3], 2, 2,'ppsi'),11, 2, d[3]],
                                 [tpppi* self.b( 7, 1,d[2], 1, 1,'pppi'), 7, 1, d[2]],
                                 [tppsi* self.b( 7, 1,d[2], 1, 1,'ppsi'), 7, 1, d[2]],
                                 [tpppi* self.b(10, 1,d[3], 1, 1,'pppi'),10, 1, d[3]],
                                 [tppsi* self.b(10, 1,d[3], 1, 1,'ppsi'),10, 1, d[3]],
                                 [tpppi* self.b( 6, 3,d[1], 3, 3,'pppi'), 6, 3, d[1]],
                                 [tppsi* self.b( 6, 3,d[1], 3, 3,'ppsi'), 6, 3, d[1]],
                                 [tpppi* self.b( 9, 3,d[2], 3, 3,'pppi'), 9, 3, d[2]],
                                 [tppsi* self.b( 9, 3,d[2], 3, 3,'ppsi'), 9, 3, d[2]],

                                 [tspsi* self.b( 0, 6,d[0], 0, 3,'spsi'), 0, 6, d[0]],
                                 [tspsi* self.b( 0, 9,d[0], 0, 3,'spsi'), 0, 9, d[0]],
                                 [tppsi* self.b( 1, 6,d[0], 1, 3,'ppsi'), 1, 6, d[0]],
                                 [tpppi* self.b( 1, 6,d[0], 1, 3,'pppi'), 1, 6, d[0]],
                                 [tppsi* self.b( 2, 9,d[0], 2, 3,'ppsi'), 2, 9, d[0]],
                                 [tpppi* self.b( 2, 9,d[0], 2, 3,'pppi'), 2, 9, d[0]],
                                 [tppsi* self.b( 3, 8,d[0], 3, 2,'ppsi'), 3, 8, d[0]],
                                 [tpppi* self.b( 3, 8,d[0], 3, 2,'pppi'), 3, 8, d[0]],
                                 [tppsi* self.b( 3, 4,d[0], 3, 1,'ppsi'), 3, 4, d[0]],
                                 [tpppi* self.b( 3, 4,d[0], 3, 1,'pppi'), 3, 4, d[0]],
                                 [tspsi* self.b( 6, 0,d[1], 3, 0,'spsi'), 6, 0, d[1]],
                                 [tspsi* self.b( 9, 0,d[2], 3, 0,'spsi'), 9, 0, d[2]],
                                 [tppsi* self.b( 6, 1,d[1], 3, 1,'ppsi'), 6, 1, d[1]],
                                 [tpppi* self.b( 6, 1,d[1], 3, 1,'pppi'), 6, 1, d[1]],
                                 [tppsi* self.b( 9, 2,d[2], 3, 2,'ppsi'), 9, 2, d[2]],
                                 [tpppi* self.b( 9, 2,d[2], 3, 2,'pppi'), 9, 2, d[2]],
                                 [tppsi* self.b( 4, 3,d[1], 1, 3,'ppsi'), 4, 3, d[1]],
                                 [tpppi* self.b( 4, 3,d[1], 1, 3,'pppi'), 4, 3, d[1]],
                                 [tppsi* self.b( 8, 3,d[2], 2, 3,'ppsi'), 8, 3, d[2]],
                                 [tpppi* self.b( 8, 3,d[2], 2, 3,'pppi'), 8, 3, d[2]],
                                 [tspsi* self.b( 0, 5,d[0], 0, 2,'spsi'), 0, 5, d[0]],
                                 [tspsi* self.b( 0, 7,d[0], 0, 1,'spsi'), 0, 7, d[0]],
                                 [tspsi* self.b( 0,10,d[0], 0, 1,'spsi'), 0,10, d[0]],
                                 [tspsi* self.b( 0,11,d[0], 0, 2,'spsi'), 0,11, d[0]],
                                 [tspsi* self.b( 5, 0,d[1], 2, 0,'spsi'), 5, 0, d[1]],
                                 [tspsi* self.b( 7, 0,d[2], 1, 0,'spsi'), 7, 0, d[2]],
                                 [tspsi* self.b(10, 0,d[3], 1, 0,'spsi'),10, 0, d[3]],
                                 [tspsi* self.b(11, 0,d[3], 2, 0,'spsi'),11, 0, d[3]],
                                 [tppsi* self.b( 1, 5,d[0], 1, 2,'ppsi'), 1, 5, d[0]],
                                 [tpppi* self.b( 1, 5,d[0], 1, 2,'pppi'), 1, 5, d[0]],
                                 [tppsi* self.b( 1, 8,d[0], 1, 2,'ppsi'), 1, 8, d[0]],
                                 [tpppi* self.b( 1, 8,d[0], 1, 2,'pppi'), 1, 8, d[0]],
                                 [tppsi* self.b( 1, 9,d[0], 1, 3,'ppsi'), 1, 9, d[0]],
                                 [tpppi* self.b( 1, 9,d[0], 1, 3,'pppi'), 1, 9, d[0]],
                                 [tppsi* self.b( 1,11,d[0], 1, 2,'ppsi'), 1,11, d[0]],
                                 [tpppi* self.b( 1,11,d[0], 1, 2,'pppi'), 1,11, d[0]],
                                 [tppsi* self.b( 1,12,d[0], 1, 3,'ppsi'), 1,12, d[0]],
                                 [tpppi* self.b( 1,12,d[0], 1, 3,'pppi'), 1,12, d[0]],
                                 [tppsi* self.b( 2, 4,d[0], 2, 1,'ppsi'), 2, 4, d[0]],
                                 [tpppi* self.b( 2, 4,d[0], 2, 1,'pppi'), 2, 4, d[0]],
                                 [tppsi* self.b( 2, 7,d[0], 2, 1,'ppsi'), 2, 7, d[0]],
                                 [tpppi* self.b( 2, 7,d[0], 2, 1,'pppi'), 2, 7, d[0]],
                                 [tppsi* self.b( 2, 6,d[0], 2, 3,'ppsi'), 2, 6, d[0]],
                                 [tpppi* self.b( 2, 6,d[0], 2, 3,'pppi'), 2, 6, d[0]],
                                 [tppsi* self.b( 2,10,d[0], 2, 1,'ppsi'), 2,10, d[0]],
                                 [tpppi* self.b( 2,10,d[0], 2, 1,'pppi'), 2,10, d[0]],
                                 [tppsi* self.b( 2,12,d[0], 2, 3,'ppsi'), 2,12, d[0]],
                                 [tpppi* self.b( 2,12,d[0], 2, 3,'pppi'), 2,12, d[0]],
                                 [tppsi* self.b( 3, 5,d[0], 3, 2,'ppsi'), 3, 5, d[0]],
                                 [tpppi* self.b( 3, 5,d[0], 3, 2,'pppi'), 3, 5, d[0]],
                                 [tppsi* self.b( 3, 7,d[0], 3, 1,'ppsi'), 3, 7, d[0]],
                                 [tpppi* self.b( 3, 7,d[0], 3, 1,'pppi'), 3, 7, d[0]],
                                 [tppsi* self.b( 3,10,d[0], 3, 1,'ppsi'), 3,10, d[0]],
                                 [tpppi* self.b( 3,10,d[0], 3, 1,'pppi'), 3,10, d[0]],
                                 [tppsi* self.b( 3,11,d[0], 3, 2,'ppsi'), 3,11, d[0]],
                                 [tpppi* self.b( 3,11,d[0], 3, 2,'pppi'), 3,11, d[0]],
                                 [tppsi* self.b( 5, 1,d[1], 2, 1,'ppsi'), 5, 1, d[1]],
                                 [tpppi* self.b( 5, 1,d[1], 2, 1,'pppi'), 5, 1, d[1]],
                                 [tppsi* self.b( 8, 1,d[2], 2, 1,'ppsi'), 8, 1, d[2]],
                                 [tpppi* self.b( 8, 1,d[2], 2, 1,'pppi'), 8, 1, d[2]],
                                 [tppsi* self.b( 9, 1,d[2], 3, 1,'ppsi'), 9, 1, d[2]],
                                 [tpppi* self.b( 9, 1,d[2], 3, 1,'pppi'), 9, 1, d[2]],
                                 [tppsi* self.b(11, 1,d[3], 2, 1,'ppsi'),11, 1, d[3]],
                                 [tpppi* self.b(11, 1,d[3], 2, 1,'pppi'),11, 1, d[3]],
                                 [tppsi* self.b(12, 1,d[3], 3, 1,'ppsi'),12, 1, d[3]],
                                 [tpppi* self.b(12, 1,d[3], 3, 1,'pppi'),12, 1, d[3]],
                                 [tppsi* self.b( 4, 2,d[1], 1, 2,'ppsi'), 4, 2, d[1]],
                                 [tpppi* self.b( 4, 2,d[1], 1, 2,'pppi'), 4, 2, d[1]],
                                 [tppsi* self.b( 7, 2,d[2], 1, 2,'ppsi'), 7, 2, d[2]],
                                 [tpppi* self.b( 7, 2,d[2], 1, 2,'pppi'), 7, 2, d[2]],
                                 [tppsi* self.b( 6, 2,d[1], 3, 2,'ppsi'), 6, 2, d[1]],
                                 [tpppi* self.b( 6, 2,d[1], 3, 2,'pppi'), 6, 2, d[1]],
                                 [tppsi* self.b(10, 2,d[3], 1, 2,'ppsi'),10, 2, d[3]],
                                 [tpppi* self.b(10, 2,d[3], 1, 2,'pppi'),10, 2, d[3]],
                                 [tppsi* self.b(12, 2,d[3], 3, 2,'ppsi'),12, 2, d[3]],
                                 [tpppi* self.b(12, 2,d[3], 3, 2,'pppi'),12, 2, d[3]],
                                 [tppsi* self.b( 5, 3,d[1], 2, 3,'ppsi'), 5, 3, d[1]],
                                 [tpppi* self.b( 5, 3,d[1], 2, 3,'pppi'), 5, 3, d[1]],
                                 [tppsi* self.b( 7, 3,d[2], 1, 3,'ppsi'), 7, 3, d[2]],
                                 [tpppi* self.b( 7, 3,d[2], 1, 3,'pppi'), 7, 3, d[2]],
                                 [tppsi* self.b(10, 3,d[3], 1, 3,'ppsi'),10, 3, d[3]],
                                 [tpppi* self.b(10, 3,d[3], 1, 3,'pppi'),10, 3, d[3]],
                                 [tppsi* self.b(11, 3,d[3], 2, 3,'ppsi'),11, 3, d[3]],
                                 [tpppi* self.b(11, 3,d[3], 2, 3,'pppi'),11, 3, d[3]]
                               ]


        self.soc_hoppings    = [ [  sBp    *-1.0j                  , 1, 2, d[0]],  # Pb onsite
                                 [  sBp    * 1.0j                  ,14,15, d[0]], 
                                 [  sBp    * 1.0                   , 1,16, d[0]], 
                                 [  sBp    *-1.0                   ,14, 3, d[0]], 
                                 [  sBp    *-1.0j                  , 2,16, d[0]], 
                                 [  sBp    *-1.0j                  ,15, 3, d[0]], 
                                 [  sAp    *-1.0j                  , 4, 5, d[0]],  # I1 onsite
                                 [  sAp    * 1.0j                  ,17,18, d[0]], 
                                 [  sAp    * 1.0                   , 4,19, d[0]], 
                                 [  sAp    *-1.0                   ,17, 6, d[0]], 
                                 [  sAp    *-1.0j                  , 5,19, d[0]], 
                                 [  sAp    *-1.0j                  ,18, 6, d[0]], 
                                 [  sAp    *-1.0j                  , 7, 8, d[0]],  # I2 onsite
                                 [  sAp    * 1.0j                  ,20,21, d[0]],
                                 [  sAp    * 1.0                   , 7,22, d[0]],
                                 [  sAp    *-1.0                   ,20, 9, d[0]],
                                 [  sAp    *-1.0j                  , 8,22, d[0]],
                                 [  sAp    *-1.0j                  ,21, 9, d[0]],
                                 [  sAp    *-1.0j                  ,10,11, d[0]],  # I3 onsite
                                 [  sAp    * 1.0j                  ,23,24, d[0]],
                                 [  sAp    * 1.0                   ,10,25, d[0]],
                                 [  sAp    *-1.0                   ,23,12, d[0]],
                                 [  sAp    *-1.0j                  ,11,25, d[0]],
                                 [  sAp    *-1.0j                  ,24,12, d[0]],
                               ]


    def b(self, orb1, orb2, orb2_R, type1, type2, bond_type):
        v = {0:np.array([0,0,0]),1:np.array([1.0,0,0]),2:np.array([0,1.0,0]),3:np.array([0,0,1.0])}
        v1 = v[type1]
        v2 = v[type2]
        orb1_coord = np.dot( self.orbitals[orb1,:],self.lattice )
        orb2_coord = np.dot( self.orbitals[orb2,:]+np.array(orb2_R),  self.lattice )
        orb1_high_coord  =  np.dot( self.high_orbitals[orb1,:],self.high_lattice )
        orb2_high_coord  =  np.dot( (self.high_orbitals[orb2,:]+np.array(orb2_R)),self.high_lattice )
        distance = np.linalg.norm( orb2_coord - orb1_coord )
        distance_high = np.linalg.norm( orb2_high_coord - orb1_high_coord )
        factor = exp(-1.0*self.alpha[bond_type]*(distance-distance_high))
        sign_hop, proj = t_sign( orb1_coord, orb2_coord, v1, v2, bond_type )
       #print orb1, orb2, bond_type,  factor, distance, distance_high, proj
        return factor * proj * sign_hop



def t_sign(orb1,orb2,v1,v2,bond_type):
    orb1_orb2_vector = orb2 - orb1
    norm_orb1_orb2_vector = orb1_orb2_vector / np.linalg.norm( orb1_orb2_vector )
    if np.all(norm_orb1_orb2_vector <=1e-5 ): raise Exception('Hoppings between the same orbitals, %s, %s, %s'%(np.array_str(orb1),np.array_str(orb2),bond_type))
    if bond_type == 'spsi':
        if np.dot( v1, norm_orb1_orb2_vector ) == 0: proj = abs( np.dot( v2, norm_orb1_orb2_vector ) )
        if np.dot( v2, norm_orb1_orb2_vector ) == 0: proj = abs( np.dot( v1, norm_orb1_orb2_vector ) )
        if abs(proj)>= 1e-5:
            v1r = np.sign( np.dot( v1,     orb1_orb2_vector ) )
            v2r = np.sign( np.dot( v2, -1.0*orb1_orb2_vector) )
            if v1r == 0: v1r = 1.0
            if v2r == 0: v2r = 1.0
            if np.sign( v1r * v2r ) < 0: sign_hop =  1.0
            if np.sign( v1r * v2r ) > 0: sign_hop = -1.0
        else:
            sign_hop = 0.0
    elif bond_type == 'ppsi':
        proj = abs( np.dot( v1, norm_orb1_orb2_vector ) ) * abs( np.dot( v2, norm_orb1_orb2_vector ) )
        if abs(proj)>= 1e-5:
            v1r = np.sign( np.dot( v1,      orb1_orb2_vector ) )
            v2r = np.sign( np.dot( v2, -1.0*orb1_orb2_vector ) )
            if np.sign( v1r * v2r ) < 0: sign_hop =  1.0
            if np.sign( v1r * v2r ) > 0: sign_hop = -1.0
        else:
            sign_hop = 0.0
    elif bond_type == 'pppi':
        v1r_tmp  = -1.0 * np.cross( np.cross( v1, orb1_orb2_vector ), orb1_orb2_vector )
        v1r_norm = np.linalg.norm( v1r_tmp )
        v2r_tmp  = -1.0 * np.cross( np.cross( v2, -1.0*orb1_orb2_vector ), -1.0*orb1_orb2_vector )
        v2r_norm = np.linalg.norm( v2r_tmp )
        if v1r_norm >= 1e-5 and v2r_norm >= 1e-5:
            v1r     = v1r_tmp / np.linalg.norm( v1r_tmp )
            v2r     = v2r_tmp / np.linalg.norm( v2r_tmp )
            proj    =  abs( np.dot( v1r, v2 ) )  *  abs( np.dot( v1r, v1 ) )
            if np.sign( np.dot( v1r, v2r ) ) < 0 and abs(proj) >= 1e-5: 
                sign_hop = -1.0
            elif np.sign( np.dot( v1r, v2r ) ) > 0 and abs(proj) >= 1e-5: 
                sign_hop =  1.0
            else:
                sign_hop = 0.0
                proj     = 0.0
        else:
            sign_hop = 0.0
            proj     = 0.0
    else:
        raise Exception('unknown bond_type')
    return sign_hop, proj


