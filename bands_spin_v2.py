#!/usr/bin/env  python2

import numpy as np
from math import sqrt
from v2_tb_rashba_soc import *
from initialization import *
from scipy.optimize import leastsq


def gen_bands_plot_kpath(kpath,evals):
    norb = np.shape(evals)[1]  # evals is transposed. now evals = [nkpt, norb]
    nkpt = np.shape(evals)[0]
    bands = np.zeros( (nkpt,norb+1) )
    bands[:,0] = kpath
    bands[:,1:] = evals
    np.savetxt('test_soc_bands_oneshot.dat',bands,fmt='%15.10f')
    return

# evc   =  [nbnd,nkpt,norb]
def print_out_orb_character(ikpt,ibnd,jbnd,evals,evc):
    norb = np.shape(evals)[1]/2
    for i in range(0,norb):
        print i , sqrt(abs(evc[ibnd,ikpt,i])**2 + abs(evc[ibnd,ikpt,i+norb])**2),  sqrt(abs(evc[jbnd,ikpt,i])**2 + abs(evc[jbnd,ikpt,i+norb])**2)


def gen_splittings(kpath,evals,ibnd,jbnd):
    norb = np.shape(evals)[1]/2    # evals is transposed. now evals = [nkpt, norb]
    nkpt = np.shape(evals)[0]
    nbnd = np.shape(evals)[1]
    splitting = np.zeros( (nkpt,2) )
    for ikpt in range(0,nkpt):
        splitting[ikpt,0] = evals[ikpt,ibnd  ] - evals[ikpt,ibnd-1] # valence
        splitting[ikpt,1] = evals[ikpt,jbnd+1] - evals[ikpt,jbnd  ] # conduction
    return np.sum( splitting, axis = 0 )/nkpt



def gen_spin_expectation_csv(kpath,evc,evals,ibnd):
    norb = np.shape(evals)[1]/2    # evals is transposed. now evals = [nkpt, norb]
    nkpt = np.shape(evals)[0]
    nbnd = np.shape(evals)[1]
    spin_mag = np.zeros( (nkpt,7) ).astype(float)
    for i in range(0,nkpt):
        spin_mag[i,0:3] = kpath[i,:]
        spin_mag[i,3:6] = solve_spin_expectation( evc, i, ibnd, norb )
        spin_mag[i,6  ] = evals[i,ibnd]
    np.savetxt('tb_2_csv_%s_.csv'%(ibnd+1), spin_mag, fmt='%15.10f',delimiter=",",comments='x,y,z,vx,vy,vz,e' )
    return
    
def average_v_dot_c_spin(h_list,kpath,evc,evals,ibnd,jbnd):
    norb = np.shape(evals)[1]/2    # evals is transposed. now evals = [nkpt, norb]
    nkpt = np.shape(evals)[0]
    nbnd = np.shape(evals)[1]
    spin_v = np.zeros( (nkpt,3) ).astype(float)
    spin_c = np.zeros( (nkpt,3) ).astype(float)
    spin_mag = np.zeros( (nkpt,4) ).astype(float)
    vortex_v = np.zeros( (nkpt,3) ).astype(float)
    vortex_c = np.zeros( (nkpt,3) ).astype(float)
    vortex_v_average = 0.0
    vortex_c_average = 0.0
    spin_mag_sum  = 0.0
    spin_mag_sum_std  = 0.0
    spin_mag_core = 0.0
    core_kpt = 0
    for i in range(0,nkpt):
        spin_v[i,:] = solve_spin_expectation( evc, i, ibnd, norb )  # valence
        spin_c[i,:] = solve_spin_expectation( evc, i, jbnd, norb )  # conduction
        sdotv = np.dot( spin_v[i,:], spin_c[i,:] )/(np.linalg.norm(spin_v[i,:])*np.linalg.norm(spin_c[i,:]))
        if sdotv < -1:  sdotv = -0.9999
        if sdotv >  1:  sdotv =  0.9999
        spin_mag[i,0] = np.rad2deg(np.arccos(sdotv))
        spin_mag[i,1:] = kpath[i,:]
        if np.linalg.norm(kpath[i,0]-np.array([0.5,0.5,0.5])) <= 0.15:
            core_kpt += 1
            spin_mag_core += spin_mag[i,0]
        vortex_v[i,:] = np.cross( spin_v[i,:],kpath[i,:] )
        vortex_c[i,:] = np.cross( spin_c[i,:],kpath[i,:] )
    spin_mag_sum = np.sum(spin_mag[:,0])/nkpt
    spin_mag_sum_std = np.std(spin_mag[:,0])
    vortex_v_average = np.sum( vortex_v, axis=0 )/nkpt 
    vortex_c_average = np.sum( vortex_c, axis=0 )/nkpt 
    vortex_v_average = vortex_v_average/np.linalg.norm( vortex_v_average )
    vortex_c_average = vortex_c_average/np.linalg.norm( vortex_c_average )
    return spin_mag_sum, spin_mag_sum_std, spin_mag_core/core_kpt, vortex_v_average, vortex_c_average



def read_k_grid( filename ):
    return np.loadtxt( filename, dtype=float )

def read_dft_bands( filename ):
    return np.loadtxt( filename, dtype=float )

#  eval  =  [norb,nkpt]
#  evc   =  [norb,nkpt,norb]
def solve_tb_model( h_list, kpt, lattice, orbitals, onsite  ):
    para = parasinitial( h_list, lattice=lattice, orbitals=orbitals, onsite=onsite  )
    tb_model = tb_soc( para.norb, para.lattice, para.orbitals, kpt, para.onsite, para.t_hoppings, lorb=para.soc_hoppings,soc=True, eigenvector=True, auto_orb=True )
    tb_model.solve_all()
    print '*************'
    print h_list
    print '*************'
    return np.transpose( tb_model._eval )

# eval  =  [norb,nkpt]
# evc   =  [norb,nkpt,norb]
def solve_tb_model_evc( h_list, kpt, lattice, orbitals, onsite  ):
    para = parasinitial( h_list, lattice=lattice, orbitals=orbitals, onsite=onsite  )
    tb_model = tb_soc( para.norb, para.lattice, para.orbitals, kpt, para.onsite, para.t_hoppings, lorb=para.soc_hoppings,soc=True, eigenvector=True, auto_orb=True )
    tb_model.solve_all(eig_vectors=True)
    return np.transpose( tb_model._eval ), tb_model._evc

def solve_spin_expectation( evc, ikpt, ibnd, norb ):
    sigma_x = 0.0+0.0j
    sigma_y = 0.0+0.0j
    sigma_z = 0.0+0.0j
    for jorb in range(0, norb):
        sigma_x += evc[ibnd,ikpt,jorb+norb].conjugate() * evc[ibnd,ikpt,jorb]        +  evc[ibnd,ikpt,jorb].conjugate() * evc[ibnd,ikpt,jorb+norb]
        sigma_y += evc[ibnd,ikpt,jorb+norb].conjugate() * evc[ibnd,ikpt,jorb] *1.0j  -  evc[ibnd,ikpt,jorb].conjugate() * evc[ibnd,ikpt,jorb+norb] * 1.0j
        sigma_z += evc[ibnd,ikpt,jorb].conjugate() * evc[ibnd,ikpt,jorb]        -  evc[ibnd,ikpt,jorb+norb].conjugate() * evc[ibnd,ikpt,jorb+norb]
    return np.array([sigma_x.real, sigma_y.real, sigma_z.real])/2.0

calc_residual = lambda h_list, kgrid, lattice, orbitals, onsite, dft_bands: np.ravel( solve_tb_model( h_list, kgrid, lattice, orbitals, onsite ) - dft_bands   )


# input files
kgrid         = read_k_grid( sys.argv[1] )
kpath         = read_k_grid( sys.argv[2] )


# initialization
lattice       = np.loadtxt(sys.argv[3],dtype=float)
orbitals      = np.loadtxt(sys.argv[4],dtype=float)

onsite        = np.array([-4.434, 3.8968,3.8968 ,3.8968 ,0.83209 ,0.83209 ,0.83209 ,0.83029 ,0.83209 ,0.83209 , 0.8369,0.8369 ,0.8369 ]).astype(float)
h_list        = np.array( [ ] )


leasq       = False
gen_bands   = True
gen_orb_cha = False
gen_ss      = False
gen_split   = False
gen_csv     = False


# Gen leasq opt
# use leqsq to optimize parameters
if leasq == True:
    dft_bands = read_dft_bands( 'e_bands' )
    h_list_fit, success    = leastsq(calc_residual, h_list, args=( kgrid, lattice, orbitals, onsite ,dft_bands), maxfev=1000)
    print '=======Summary====='
    tb_bands = solve_tb_model( h_list_fit, kgrid, lattice, orbitals, onsite )


# Gen bands
# add for plot band structure
if not (kpath is None) and gen_bands == True:
    nkpt  = np.shape(kpath)[0]
    k     = np.linspace(0,nkpt-1,num=nkpt)
    tb_bands, tb_evc  = solve_tb_model_evc( h_list, kpath, lattice, orbitals, onsite )
    ibnd = 20 - 1
    jbnd = 21 -1
    print 'Band gap ', np.amin(tb_bands[:,jbnd]) - np.amax(tb_bands[:,ibnd]), 'C_k ', np.argmin( tb_bands[:nkpt/2,jbnd] ), 'V_k ',  np.argmax( tb_bands[:nkpt/2,ibnd] )
    gen_bands_plot_kpath(k ,tb_bands)



# add for print orbital characters
if not (kpath is None) and gen_orb_cha == True and gen_bands == True:
    ibnd = 20 - 1
    jbnd = 21 - 1
    ikpt_v = np.argmax( tb_bands[:nkpt/2,ibnd] )
    ikpt_c = np.argmin( tb_bands[:nkpt/2,jbnd] )
    print  'valence top is', ikpt_v
    print_out_orb_character(ikpt_v,ibnd,jbnd,tb_bands,tb_evc)
    print  'conduct bot is', ikpt_c
    print_out_orb_character(ikpt_c,ibnd,jbnd,tb_bands,tb_evc)



# Gen S \dot S
# add for print S \dot S for valence and conduction 
if not (kgrid is None) and gen_ss == True:
    ibnd = 20 - 1
    jbnd = 21 - 1
    tb_bands2, tb_evc2 = solve_tb_model_evc( h_list, kgrid, lattice, orbitals, onsite )
    spin_mag_sum, spin_mag_sum_std, spin_mag_core, vortex_v_average, vortex_c_average = average_v_dot_c_spin(h_list ,kgrid,tb_evc2,tb_bands2,ibnd,jbnd)



# Gen C & V splitting 
# add for print out the average splittings for C and V
if gen_split == True and gen_ss == True:
    splitting = gen_splittings(kgrid,tb_bands2,ibnd,jbnd)
    print 'BZ_ave: %15.10f   BZ_std: %15.10f   BZ_core: %15.10f  %s  V %s  C %s'%(spin_mag_sum, spin_mag_sum_std, spin_mag_core, np.array_str(splitting), np.array_str(vortex_v_average),np.array_str(vortex_c_average))



# Gen csv
# add for generate csv files for spin expectation plot
if not( kgrid is None) and gen_csv == True and gen_ss == True:
    gen_spin_expectation_csv(kgrid,tb_evc2,tb_bands2,ibnd)
    gen_spin_expectation_csv(kgrid,tb_evc2,tb_bands2,jbnd)


#
#
#
#
